# Victor Smith Portfolio Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to
[Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.2] - 2024-07-21

- react-slick was implemented was a carousel.
- Route `Astronomy` was deleted.
- Astonomy page was deleted.
- Not found page was modified

## [1.0.1] - 2024-07-21

- Issue with intl has fixed.
