export const generalData = {
  imagebaseurl: 'https://github.com/Victor-Smith-Dev',
  name: 'Victor Smith',
  role: 'Full Stack Developer',
  linkedinId: 'victor-smith-developer',
  skypeid: 'Your skypeid',
  roleDescription:
    'I rate my experience as medium level in the field of software development, however I know the tools and have the potential to improve.',
  aboutme:
    "My name is Victor Smith, Software Developer, with a degree in Computer Engineering and Computing, graduated from the National Experimental University of Guyana. With 8 years of work experience in software development teams, which have allowed them to acquire skills in mobile programming and obtain a full-stack developer profile, consolidating leadership skills, proactivity, empathy, positive attitude, adaptability and resolution. from problems. I currently have experience in areas such as mining, banking, DAM's (Digital Assets Management), e-commerce, social assistance systems, horizontal properties and consolidated cargo transportation.",
  address: 'RM Santiago of Chile, Chile',
  website: 'https://github.com/Victor-Smith-Dev',
  skillsDescription: '',
  profilePic: 'https://s3.amazonaws.com/vs.uploads/2022/dec/profilepic.jpeg',
};

export const socialLinks = [
  {
    name: 'linkedin',
    url: 'https://www.linkedin.com/in/victor-smith-developer/',
    className: 'fa fa-linkedin',
  },
  {
    name: 'github',
    url: 'https://github.com/Victor-Smith-Dev',
    className: 'fa fa-github',
  },
  {
    name: 'twitter',
    url: 'https://twitter.com/V_Smith90',
    className: 'fa fa-twitter',
  },
];

export const education = [
  {
    UniversityName: 'Universidad Nacional Experimental de Guayana',
    specialization: 'Informatics Engineer',
    MonthOfPassing: 'Jul',
    YearOfPassing: '2016',
    Achievements:
      'https://drive.google.com/file/d/1Fr3Smg7yhPH_4vArlBng-wZfDAckG1Rq/view?usp=share_link',
  },
  {
    UniversityName: 'Universidad Nacional Experimental de Guayana',
    specialization: 'computer technologist',
    MonthOfPassing: 'Oct',
    YearOfPassing: '2014',
    Achievements:
      'https://drive.google.com/file/d/1Lij7fqFzNIQMikCHgnKHGo0R35xMoFLq/view?usp=share_link',
  },
  {
    UniversityName: 'U.E San Juan Apóstol',
    specialization: 'Bachelor of Science',
    MonthOfPassing: 'Jul',
    YearOfPassing: '2007',
    Achievements:
      'https://drive.google.com/file/d/1d6s3zO-uanNOY6mH8RIa3e5WuQYpZ40l/view?usp=share_link',
  },
];

export const work = [
  {
    CompanyName: 'Falabella',
    specialization: 'Full Stack Software Engineer',
    MonthOfLeaving: 'Current',
    YearOfLeaving: '',
    Achievements:
      'AWS Essential Techniques Practical Workshop - AWS, Node, ReactJs, Express, Typescript, Nest.JS, Fastify, Python, GCP, Prisma',
  },
  {
    CompanyName: 'Cencosud S.A',
    specialization: 'Software Engineer',
    MonthOfLeaving: 'Jul',
    YearOfLeaving: '2023',
    Achievements:
      'AWS Essential Techniques Practical Workshop - AWS, Node, ReactJs, Express, Typescript, Nest.JS',
  },
  {
    CompanyName: 'BC Tecnología',
    specialization: 'Desarrollador full stack',
    MonthOfLeaving: 'Jul',
    YearOfLeaving: '2022',
    Achievements: 'AWS, Node, ReactJs, Express, Typescript, Nest.Js',
  },
  {
    CompanyName: 'Accenture',
    specialization: 'Tech Arch Delivery Analyst',
    MonthOfLeaving: 'Nov',
    YearOfLeaving: '2021',
    Achievements:
      'I made an important contribution in the banking sector - AWS, Node, ReactJs, Express, Typescript, Titanium, Nest.js',
  },
  {
    CompanyName: 'Bunkey',
    specialization: 'Full Stack Developer',
    MonthOfLeaving: 'Mar',
    YearOfLeaving: '2019',
    Achievements:
      'In charge of writing clean code in the client, in such a way that when advancing in the scaling of the front-end, the organization and modularity of the code prevail. AWS, Node, ReactJs, Express, Mongo DB',
  },
  {
    CompanyName: 'ZEKE LTDA',
    specialization: 'Project Engineer',
    MonthOfLeaving: 'Nov',
    YearOfLeaving: '2019',
    Achievements:
      'Proactively identify technical project risks; Participate in meetings with the client; Communicate project progress; Report business doubts to the analyst. -  Laravel, Cake PHP, Wordpress, Oracle DB',
  },
];

export const skills = [
  {
    skillname: 'React JS',
    points: '80%',
  },
  {
    skillname: 'SCSS',
    points: '70%',
  },
  {
    skillname: 'Node',
    points: '70%',
  },
  {
    skillname: 'Nest Js',
    points: '70%',
  },
  {
    skillname: 'Typescript',
    points: '60%',
  },
  {
    skillname: 'Express',
    points: '70%',
  },
  {
    skillname: 'AWS',
    points: '50%',
  },
  {
    skillname: 'Mongo DB',
    points: '60%',
  },
  {
    skillname: 'Maria DB',
    points: '70%',
  },
];

export const courses = [
  {
    image: 'https://s3.amazonaws.com/vs.uploads/2022/dec/courses/personal-brand.png',
    caption: 'Personal Brand',
  },
  {
    image: 'https://s3.amazonaws.com/vs.uploads/2022/dec/courses/es6-react-hooks.png',
    caption: 'ES6 React Hooks',
  },
  {
    image: 'https://s3.amazonaws.com/vs.uploads/2022/dec/courses/react-native-apps.png',
    caption: 'React Native',
  },
  {
    image: 'https://s3.amazonaws.com/vs.uploads/2022/dec/courses/gitlab-devops.png',
    caption: 'Gitlab Devops',
  },
  {
    image: 'https://s3.amazonaws.com/vs.uploads/2022/dec/courses/firebase-react-hooks.png',
    caption: 'Firebase React Hooks',
  },
  {
    image: 'https://s3.amazonaws.com/vs.uploads/2022/dec/courses/docker-course.png',
    caption: 'Docker',
  },
  {
    image: 'https://s3.amazonaws.com/vs.uploads/2022/dec/courses/basic-aws.png',
    caption: 'Basic AWS',
  },
  {
    image: 'https://s3.amazonaws.com/vs.uploads/2022/dec/courses/nest-database-persistence.png',
    caption: 'Nest Database Persistence',
  },
];
