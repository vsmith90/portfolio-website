import React from 'react';
import PropTypes from 'prop-types';

const Footer = ({ socialLinks }) => {
  const handleSocialLinks = () => (
    <React.Fragment>
      <ul className="social-links">
        {socialLinks &&
          socialLinks.map((item, index) => {
            return (
              <li key={index}>
                <a href={item.url}>
                  <i className={item.className} />
                </a>
              </li>
            );
          })}
      </ul>
    </React.Fragment>
  );

  return (
    <footer>
      <div className="row">
        <div className="twelve columns">{handleSocialLinks()}</div>
        <div id="go-top">
          <a className="smoothscroll" title="Back to Top" href="#home">
            <i className="icon-up-open" />
          </a>
        </div>
      </div>
    </footer>
  );
};

Footer.propTypes = {
  socialLinks: PropTypes.array,
};

export default Footer;
