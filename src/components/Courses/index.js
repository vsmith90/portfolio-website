import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import Carousel from '../Common/carousel.component';

const Courses = ({ courses }) => {
  const { t } = useTranslation('common');

  const handleCourses = () => (
    <React.Fragment>
      {courses.map((course, index) => (
        <div className="box" key={index}>
          <img src={course.image} alt={course.caption} />
          <span>{course.caption}</span>
        </div>
      ))}
    </React.Fragment>
  );

  return (
    <section id="courses">
      <div className="row row-portafolio">
        <div className="twelve columns collapsed">
          <h1>{t('courses.title')}</h1>
          <div className="courses-container">{handleCourses()}</div>
        </div>
      </div>
    </section>
  );
};

Courses.propTypes = {
  courses: PropTypes.array,
};

export default Courses;
