import React from 'react';
import ReactDOM from 'react-dom';
import Courses from '.';

const courses = [];

it('renders without crashing <Courses />', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Courses courses={courses} />, div);
});
