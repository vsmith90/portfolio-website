import React from 'react';
import ReactDOM from 'react-dom';
import ContactUs from '.';

const generalData = {};

it('renders without crashing <ContactUs />', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ContactUs generalData={generalData} />, div);
});
