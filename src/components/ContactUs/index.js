import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

const ContactUs = ({ generalData }) => {
  const { t } = useTranslation('common');

  return (
    <>
      <section id="contact">
        <div className="row section-head">
          <div className="ten columns">
            <p className="lead">{t('contactUs.title')}</p>
          </div>
        </div>
        <div className="row">
          <aside className="eigth columns footer-widgets">
            <div className="widget">
              <h4>Linkedin: {generalData.linkedinId}</h4>
            </div>
          </aside>
        </div>
      </section>
    </>
  );
};

ContactUs.propTypes = {
  generalData: PropTypes.object,
};

export default ContactUs;
