import React from 'react';
import ReactDOM from 'react-dom';
import Testimonials from '.';

const testimonials = [];

it('renders without crashing <Testimonials />', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Testimonials testimonials={testimonials} />, div);
});
