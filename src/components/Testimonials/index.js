import React from 'react';
import { useTranslation } from 'react-i18next';

const Testimonials = () => {
  const { t, ready } = useTranslation('common');

  const handleTestimonials = (_ready) => {
    if (!_ready) return 'loading translations...';
    const items = t('testimonials.items', { returnObjects: true }).reverse();
    return (
      <React.Fragment>
        <ul className="slides">
          {items.map((_item, _index) => (
            <li key={_index}>
              <blockquote>
                <p>{_item.text}</p>
                <cite>{_item.name}</cite>
              </blockquote>
            </li>
          ))}
        </ul>
      </React.Fragment>
    );
  };

  return (
    <React.Fragment>
      <section id="testimonials">
        <div className="text-container">
          <div className="row">
            <div className="two columns header-col">
              <h1>
                <span>{t('testimonials.title')}</span>
              </h1>
            </div>
            <div className="ten columns flex-container">
              <div className="flexslider">{handleTestimonials(ready)}</div>
            </div>
          </div>
        </div>
      </section>
    </React.Fragment>
  );
};

Testimonials.propTypes = {};

export default Testimonials;
