import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

const About = ({ generalData }) => {
  const { t } = useTranslation('common');

  return (
    <>
      <section id="about">
        <div className="row">
          <div className="three columns">
            <img className="profile-pic" src={generalData.profilePic} alt="profile-pic" />
          </div>

          <div className="nine columns main-col">
            <h2>{t('about.title')}</h2>
            <p>{t('about.aboutme')}</p>

            <div className="row">
              <div className="columns contact-details">
                <h2>{t('about.subtitle')}</h2>
                <p className="address">
                  <span>{generalData.name}</span>
                  <br />
                  <span>{t('general.country')}</span>
                  <br />
                  <span>{generalData.website}</span>
                </p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
};

About.propTypes = {
  generalData: PropTypes.object,
};

export default About;
