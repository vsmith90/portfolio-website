import React from 'react';
import ReactDOM from 'react-dom';
import About from '.';

const generalData = {};

it('renders without crashing <About />', () => {
  const div = document.createElement('div');
  ReactDOM.render(<About generalData={generalData} />, div);
});
