import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import Carousel from '../Common/carousel.component';
import Card from '../Common/card.component';

const Portfolio = () => {
  const { t, ready } = useTranslation('common');

  const handlePortfolio = (_ready) => {
    if (!_ready) return 'loading portfolio...';
    const items = t('portfolio.items', { returnObjects: true });
    const carouselItems = items.map((_item, _index) => (
      <Card
        key={_index}
        title={_item.title}
        description={_item.description}
        imagePath={_item.imagePath}
      />
    ));
    return <Carousel items={carouselItems} centerMode={true} dots={true} infinite={true} autoplay={true}/>;
  };

  return (
    <>
      <section id="portfolio">
        <div className="row row-portafolio">
          <div className="twelve columns collapsed">
            <h1>{t('portfolio.title')}</h1>
            {handlePortfolio(ready)}
          </div>
        </div>
      </section>
    </>
  );
};

Portfolio.propTypes = {
  portfolio: PropTypes.array,
};

export default Portfolio;
