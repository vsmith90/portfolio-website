import React from 'react';
import ReactDOM from 'react-dom';
import Portfolio from '.';

const portfolio = [];

it('renders without crashing <Portfolio />', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Portfolio portfolio={portfolio} />, div);
});
