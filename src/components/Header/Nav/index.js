import Proptypes from 'prop-types';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import { addStoreItem, getStoreItem } from '../../../utils';

const Nav = ({ links }) => {
  const { t, i18n } = useTranslation('common');
  const language = getStoreItem('language');

  const handleChangeLanguage = (key, lang) => {
    i18n.changeLanguage(lang);
    addStoreItem(key, lang);
  };

  const handleNav = () => (
    <React.Fragment>
      <li className="current">
        {links && (
          <a className="smoothscroll" href="#home">
            {t('header.links.home')}
          </a>
        )}
        {!links && <Link to="/">{t('header.links.home')}</Link>}
      </li>
      {links &&
        links.map((link, index) => (
          <li key={index}>
            <a className="smoothscroll" href={link.url}>
              {t(link.text)}
            </a>
          </li>
        ))}
      <li>
        <button
          className="vs-button"
          onClick={() => handleChangeLanguage('language', language === 'es' ? 'en' : 'es')}>
          <i className="fa fa-language" aria-hidden="true"></i> {language}
        </button>
      </li>
    </React.Fragment>
  );

  return (
    <nav id="nav-wrap">
      <a className="mobile-btn" href="#nav-wrap" title="Show navigation">
        {t('header.navegation.show')}
      </a>
      <a className="mobile-btn" href="/#" title="Hide navigation">
        {t('header.navegation.hide')}
      </a>
      <ul id="nav" className="nav">
        {handleNav()}
      </ul>
    </nav>
  );
};

Nav.propTypes = {
  links: Proptypes.array,
};

export default Nav;
