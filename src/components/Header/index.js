import React from 'react';
import Proptypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { navLinks } from '../../config/';
import Nav from './Nav';

const Header = ({ generalData, socialLinks }) => {
  const { t } = useTranslation('common');

  const handleSocialLinks = () => {
    return (
      socialLinks &&
      socialLinks.map((item) => {
        return (
          <li key={item.name}>
            <a href={item.url} target="_blank" rel="noopener noreferrer">
              <i className={item.className} />
            </a>
          </li>
        );
      })
    );
  };

  return (
    <>
      <header id="home">
        <Nav links={navLinks} />
        <div className="row banner">
          <div className="banner-text">
            <h1 className="responsive-headline">
              {t('general.iam')} {generalData.name}.
            </h1>
            <h3 style={{ color: '#fff', fontFamily: 'sans-serif ' }}>
              {t('general.iam')} {generalData.role}. {t('general.roleDescription')}
            </h3>
            <hr />
            <ul className="social">{handleSocialLinks()}</ul>
          </div>
        </div>
        <p className="scrolldown">
          <a className="smoothscroll" href="#about">
            <i className="icon-down-circle" />
          </a>
        </p>
      </header>
    </>
  );
};

Header.propTypes = {
  generalData: Proptypes.object,
  socialLinks: Proptypes.array,
};

export default Header;
