import React from 'react';
import ReactDOM from 'react-dom';
import Header from '.';

const socialLinks = [];
const generalData = {};

it('renders without crashing <Header />', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Header generalData={generalData} socialLinks={socialLinks} />, div);
});
