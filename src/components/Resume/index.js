import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

const Resume = ({ generalData, education, work, skills }) => {
  const { t } = useTranslation('common');

  const handleEducation = () => (
    <>
      {education &&
        education.map((item, index) => (
          <div className="row item" key={index}>
            <div className="twelve columns">
              <h3>{item.UniversityName}</h3>
              <p className="info">
                {item.specialization}
                <span>&bull;</span>
                <em className="date">
                  {item.MonthOfPassing} {item.YearOfPassing}
                </em>
              </p>
              <p>
                <a href={item.Achievements} target="_blank" rel="noopener noreferrer">
                  <i className="fa fa-link resume-icon" aria-hidden="true"></i>
                  {t('resume.showCredential')}
                </a>
              </p>
            </div>
          </div>
        ))}
    </>
  );

  const handleWork = () => (
    <>
      {work &&
        work.map((item, index) => (
          <div className="row item" key={index}>
            <div className="twelve columns">
              <h3>{item.CompanyName}</h3>
              <p className="info">
                {item.specialization}
                <span>&bull;</span>{' '}
                <em className="date">
                  {item.MonthOfLeaving} {item.YearOfLeaving}
                </em>
              </p>
              <p>{item.Achievements}</p>
            </div>
          </div>
        ))}
    </>
  );

  const handleSkills = () => (
    <>
      <div className="bars">
        <ul className="skills">
          {skills &&
            skills.map((item, index) => {
              return (
                <li key={index}>
                  <span
                    className={`bar-expand`}
                    style={{
                      paddingRight: item.points,
                    }}
                  />
                  <em>{item.skillname}</em>
                </li>
              );
            })}
        </ul>
      </div>
    </>
  );

  return (
    <>
      <section id="resume">
        <div className="row education">
          <div className="three columns header-col">
            <h1>
              <span>{t('resume.education')}</span>
            </h1>
          </div>
          <div className="nine columns main-col">{handleEducation()}</div>
        </div>
        <div className="row work">
          <div className="three columns header-col">
            <h1>
              <span>{t('resume.work')}</span>
            </h1>
          </div>
          <div className="nine columns main-col">{handleWork()}</div>
        </div>

        <div className="row skill">
          <div className="three columns header-col">
            <h1>
              <span>{t('resume.skills')}</span>
            </h1>
          </div>

          <div className="nine columns main-col">
            <p>{generalData.skillsDescription}</p>
            {handleSkills()}
          </div>
        </div>
      </section>
    </>
  );
};

Resume.propTypes = {
  portfolio: PropTypes.array,
};

export default Resume;
