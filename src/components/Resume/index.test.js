import React from 'react';
import ReactDOM from 'react-dom';
import Resume from '.';

const generalData = {};
const education = [];
const work = [];
const skills = [];

it('renders without crashing <Resume />', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <Resume generalData={generalData} education={education} work={work} skills={skills} />,
    div,
  );
});
