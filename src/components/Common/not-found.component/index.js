import React from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';
import styles from './style.module.scss';

const NotFound = () => {
  const history = useHistory();
  const { t } = useTranslation('common');

  const goHome = (_history) => {
    _history.push('/');
  };

  return (
    <div className={styles['not-found-component-container']}>
      <h1>{t('notFound.title')}</h1>
      <button onClick={() => goHome(history)}>
        <i class="fa fa-arrow-left" aria-hidden="true"></i> {t('notFound.back')}
      </button>
    </div>
  );
};

export default NotFound;
