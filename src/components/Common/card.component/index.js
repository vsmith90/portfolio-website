import React from 'react';
import PropTypes from 'prop-types';
import styles from './style.module.scss';

const Card = ({ imagePath, title, description, imageAlt }) => {
  return (
    <div className={styles['card-container']}>
      <div className={styles['card']}>
        <img src={imagePath} alt={imageAlt} />
        <div className={styles['container']}>
          <h4>
            <b>{title}</b>
          </h4>
          <p>{description}</p>
        </div>
      </div>
    </div>
  );
};

Card.propTypes = {
  imagePath: PropTypes.string,
  imageAlt: '',
  title: PropTypes.string,
  description: PropTypes.string,
};

Card.defaultProps = {
  imagePath: '',
  imageAlt: '',
  title: '<CARD_TITLE>',
  description: '<CARD_DESCRIPTION>',
};

export default Card;
