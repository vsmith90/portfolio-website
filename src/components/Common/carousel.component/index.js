import React from 'react';
import Slider from 'react-slick';
import PropTypes from 'prop-types';
import styles from './style.module.scss';
import { carouselSettings } from '../../../config';

const Carousel = ({
  dots,
  infinite,
  speed,
  slidesToScroll,
  slidesToShow,
  title,
  items,
  responsive,
  centerMode,
  autoplay,
  autoplaySpeed,
}) => {
  const sliderRef = React.useRef(null);

  return (
    <div className={styles['carousel-container']}>
      {title && title.length > 0 && <h1>{title}</h1>}
      <Slider
        dots={dots}
        infinite={infinite}
        speed={speed}
        slidesToShow={slidesToShow}
        slidesToScroll={slidesToScroll}
        ref={sliderRef}
        responsive={responsive}
        centerMode={centerMode}
        autoplay={autoplay}
        autoplaySpeed={autoplaySpeed}>
        {items.map((_item, _index) => (
          <div key={_index}>{_item}</div>
        ))}
      </Slider>
    </div>
  );
};

Carousel.propTypes = {
  items: PropTypes.array,
  title: PropTypes.string,
  dots: PropTypes.bool,
  infinite: PropTypes.bool,
  speed: PropTypes.number,
  slidesToShow: PropTypes.number,
  slidesToScroll: PropTypes.number,
  responsive: PropTypes.array,
  centerMode: PropTypes.bool,
  autoplay: PropTypes.bool,
  autoplaySpeed: PropTypes.number,
};

Carousel.defaultProps = {
  items: [],
  title: '',
  dots: false,
  infinite: false,
  speed: 2000,
  slidesToShow: 4,
  slidesToScroll: 3,
  centerMode: false,
  autoplay: false,
  autoplaySpeed: 2000,
  responsive: carouselSettings.responsive,
};

export default Carousel;
