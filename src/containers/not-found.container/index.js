import React from 'react';
import NotFound from '../../components/Common/not-found.component';

const NotFoundContainer = () => {
  return <NotFound />;
};

export default NotFoundContainer;
