import React from 'react';
import ReactDOM from 'react-dom';
import { I18nextProvider } from 'react-i18next';
import i18next from 'i18next';
import './index.css';
import App from './App';
import enCommon from './config/translations/en/common.json';
import esCommon from './config/translations/es/common.json';
import { getStoreItem } from './utils';

const language = getStoreItem('language');

i18next.init({
  interpolation: { escapeValue: false },
  lng: language || 'en',
  resources: {
    en: {
      common: enCommon,
    },
    es: {
      common: esCommon,
    },
  },
});

ReactDOM.render(
  <React.StrictMode>
    <I18nextProvider i18n={i18next}>
      <App />
    </I18nextProvider>
  </React.StrictMode>,
  document.getElementById('root'),
);
