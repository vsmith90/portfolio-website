import Home from '../pages/Home';
import NotFound from '../pages/NotFound';

export const routes = [
  {
    url: '/',
    component: Home,
  },
  {
    url: '*',
    component: NotFound,
  },
];
