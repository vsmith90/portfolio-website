import React from 'react';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import { routes } from './routes';

const Routes = () => {
  return (
    <Router>
      <Switch>
        {routes.map(({ url, component }, index) => (
          <Route exact path={url} component={component} key={index} />
        ))}
      </Switch>
    </Router>
  );
};

export default Routes;
