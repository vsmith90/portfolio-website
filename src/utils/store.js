export const addStoreItem = (key, item) => {
  localStorage.setItem(key, JSON.stringify(item));
};

export const getStoreItem = (key) => {
  return JSON.parse(localStorage.getItem(key));
};

export const removeStoreItem = (key) => {
  localStorage.removeItem(key);
};
