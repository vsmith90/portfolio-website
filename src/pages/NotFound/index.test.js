import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import NotFound from '.';

it('renders without crashing <NotFound />', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <Router>
      <NotFound />
    </Router>,
    div,
  );
});
