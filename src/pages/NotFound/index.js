import React from 'react';
import NotFoundContainer from '../../containers/not-found.container';

const NotFoundPage = () => {
  return <NotFoundContainer />;
};

export default NotFoundPage;
