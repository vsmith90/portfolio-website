import React from 'react';
import About from '../../components/About';
import ContactUs from '../../components/ContactUs';
import Courses from '../../components/Courses';
import Footer from '../../components/Footer';
import Header from '../../components/Header';
import Portfolio from '../../components/Portfolio';
import Resume from '../../components/Resume';
import Testimonials from '../../components/Testimonials';
import {
  generalData,
  education,
  work,
  skills,
  courses,
  socialLinks,
} from '../../resumeData';

const Home = () => {
  return (
    <React.Fragment>
      <Header generalData={generalData} socialLinks={socialLinks} />
      <Courses courses={courses} />
      <About generalData={generalData} />
      <Resume generalData={generalData} education={education} work={work} skills={skills} />
      <Portfolio />
      <Testimonials />
      <ContactUs generalData={generalData} />
      <Footer socialLinks={socialLinks} />
    </React.Fragment>
  );
};

export default Home;
