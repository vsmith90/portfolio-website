import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import Home from '.';

it('renders without crashing <Home />', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <Router>
      <Home />
    </Router>,
    div,
  );
});
