export const portfolioStatic = {
  slideNumberStyle: {
    fontSize: '20px',
    fontWeight: 'bold',
  },
  captionStyle: {
    fontSize: '2em',
    fontWeight: 'bold',
  },
  time: 2000,
  width: '1500px',
  height: '500px',
  radius: '10px',
  slideNumber: false,
  captionPosition: 'center',
  automatic: true,
  dots: true,
  pauseIconColor: 'white',
  pauseIconSize: '40px',
  slideBackgroundColor: 'darkgrey',
  slideImageFit: 'cover',
  thumbnails: false,
  thumbnailWidth: '100px',
  style: {
    textAlign: 'center',
    maxWidth: '1500px',
    margin: '40px auto',
  },
};

export const navLinks = [
  {
    url: '#courses',
    text: 'header.links.courses',
  },
  {
    url: '#about',
    text: 'header.links.about',
  },
  {
    url: '#resume',
    text: 'header.links.resume',
  },
  {
    url: '#portfolio',
    text: 'header.links.works',
  },
  {
    url: '#testimonials',
    text: 'header.links.testimonials',
  },
  {
    url: '#contact',
    text: 'header.links.contact',
  },
];

export const carouselSettings = {
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true,
      },
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        initialSlide: 2,
      },
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      },
    },
  ],
};
