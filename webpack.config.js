const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin');

const postcssPlugins = [['postcss-preset-env', {}]];
const distFolderName = 'dist';

module.exports = {
  entry: path.resolve(__dirname, 'src/index.js'),
  output: {
    path: path.resolve(__dirname, distFolderName),
    filename: 'bundle.js',
    publicPath: '/',
    clean: true,
  },
  devServer: {
    hot: true,
    historyApiFallback: true,
    port: 3000,
    static: {
      directory: path.join(__dirname, 'public'),
    },
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {},
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
            },
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true,
              postcssOptions: {
                plugins: postcssPlugins,
              },
            },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
              sassOptions: {
                loadPath: ['src/app'],
              },
            },
          },
        ],
      },
      {
        test: /\.html$/,
        exclude: /index.html$/,
        use: [
          'file-loader?name=[contenthash]-[name].[ext]',
          'extract-loader',
          {
            loader: 'html-loader?interpolate',
            options: {
              minimize: true,
              removeComments: true,
            },
          },
        ],
      },
    ],
  },
  resolve: {
    extensions: ['*', '.js', '.jsx'],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(__dirname, 'public', 'index.html'),
      filename: 'index.html',
    }),
    new MiniCssExtractPlugin({
      filename: '[contenthash]-styles-v2.css',
      chunkFilename: '[contenthash]-[id]-v2.css',
      ignoreOrder: true,
    }),
    new CopyPlugin({
      patterns: [
        {
          from: path.join(__dirname, 'public/css'),
          to:  path.join(__dirname,  `${distFolderName}/css`)
        },
        {
          from: path.join(__dirname, 'public/js'),
          to:  path.join(__dirname,  `${distFolderName}/js`)
        },
        {
          from: path.join(__dirname, 'public/images'),
          to:  path.join(__dirname,  `${distFolderName}/images`)
        },
        {
          from: path.join(__dirname, 'public/favicon.ico'),
          to:  path.join(__dirname,  `${distFolderName}/favicon.ico`)
        },
        {
          from: path.join(__dirname, 'public/manifest.json'),
          to:  path.join(__dirname,  `${distFolderName}/manifest.json`)
        },
        {
          from: path.join(__dirname, 'public/robot.txt'),
          to:  path.join(__dirname,  `${distFolderName}/robot.txt`)
        },
      ],
    }),
  ],
};
