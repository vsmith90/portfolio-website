const esModules = ['uuid'].join('|');

const config = {
  verbose: true,
  testEnvironment: 'jsdom',
  moduleDirectories: ['node_modules'],
  coveragePathIgnorePatterns: ['/node_modules/'],
  transformIgnorePatterns: ['/node_modules/', `/node_modules/(?!${esModules})`],
  verbose: false,
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$': '<rootDir>/__mocks__/file-mock.js',
    '\\.(css|less|scss|sass)$': '<rootDir>/__mocks__/style-mock.js',
    '\\.(html)$': '<rootDir>/__mocks__/html-mock.js',
  },
  setupFilesAfterEnv: ['<rootDir>/__mocks__/setup-jest.js'],
};

module.exports = config;
